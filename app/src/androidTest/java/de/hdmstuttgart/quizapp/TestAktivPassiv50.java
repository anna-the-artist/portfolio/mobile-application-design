package de.hdmstuttgart.quizapp;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

import androidx.test.ext.junit.rules.ActivityScenarioRule;

import org.junit.Rule;
import org.junit.Test;

//@RunWith(AndroidJUnit4.class)
public class TestAktivPassiv50 {


    @Rule
    public ActivityScenarioRule<HomeActivity> activityScenarioRule
            = new ActivityScenarioRule<>(HomeActivity.class);

    @Test
    public void test3() {

        onView(withId(R.id.btnStartQuestion))
                .perform(click());

        onView(withId(R.id.btnAktivPassiv))
                .perform(click());

        onView(withId(R.id.radioBtn4))
                .perform(click());

        onView(withId(R.id.btnNext))
                .perform(click());

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        onView(withId(R.id.radioBtn2))
                .perform(click());

        onView(withId(R.id.btnNext))
                .perform(click());

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        onView(withId(R.id.radioBtn1))
                .perform(click());

        onView(withId(R.id.btnNext))
                .perform(click());

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        onView(withId(R.id.radioBtn1))
                .perform(click());

        onView(withId(R.id.btnNext))
                .perform(click());

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        onView(withId(R.id.right_view))
                .check(matches(withText("Richtig: 2")));

        onView(withId(R.id.wrong_view))
                .check(matches(withText("Falsch: 2")));

    }
}
