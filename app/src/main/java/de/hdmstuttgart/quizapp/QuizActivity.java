package de.hdmstuttgart.quizapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import de.hdmstuttgart.quizapp.database.Question;

public class QuizActivity extends AppCompatActivity {

    TextView txtQuestion;
    TextView txtViewCount;

    RadioButton radioBtn1;
    RadioButton radioBtn2;
    RadioButton radioBtn3;
    RadioButton radioBtn4;
    RadioGroup radioGroup;
    Button btnNext;

    boolean answer = false;

    //beinhaltet unsere Fragen zum jeweiligen Kategorie
    List<Question> questionList;
    Question currentQuestion;
    private ColorStateList txtColorBtn;

    private Handler handler = new Handler();

    private int questionCounter = 0;
    private int questionTotal;

    private QuizActivityViewModel viewModel;

    private int answerCorrect = 0;
    private int answerWrong = 0;

    private String overview = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);

        setup();

        txtColorBtn = radioBtn1.getTextColors();

        Intent intent = getIntent();
        overview = intent.getStringExtra("Overview");

        viewModel = ViewModelProviders.of(this).get(QuizActivityViewModel.class);
        viewModel.loadQuestions(this, overview);
        viewModel.questionDataList.observe(this, this::fetchData);
    }

    void setup(){
        //Unsere Variablen werden hier mit den jeweiligen ID´s gesetzt
        txtViewCount = findViewById(R.id.text_question_num);
        txtQuestion = findViewById(R.id.text_container_question);

        btnNext = findViewById(R.id.btnNext);

        radioGroup = findViewById(R.id.radio_group);
        radioBtn1 = findViewById(R.id.radioBtn1);
        radioBtn2 = findViewById(R.id.radioBtn2);
        radioBtn3 = findViewById(R.id.radioBtn3);
        radioBtn4 = findViewById(R.id.radioBtn4);
    }

    private void fetchData(List<Question> questions) {
        //fetchen unsere Fragen aus der Datenbank
        if(questions == null){
            return;
        }
        questionList = questions;
        startQu();
    }

    private void setQuestion(){

        radioGroup.clearCheck();
        radioBtn1.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.default_bg_button)); //nötig damit bei der nächsten frage nicht schon das voher ausgewählte feld immer noch ausgewählt ist
        radioBtn2.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.default_bg_button));
        radioBtn3.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.default_bg_button));
        radioBtn4.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.default_bg_button));

        questionTotal = questionList.size();

        //damit alle fragen aus der Kategorie ausgegeben werden, schaut er ob der counter kleiner als total ist
        if (questionCounter < questionTotal){
            currentQuestion = questionList.get(questionCounter);
            txtQuestion.setText(currentQuestion.getQuestion());
            radioBtn1.setText(currentQuestion.getOptionA());
            radioBtn2.setText(currentQuestion.getOptionB());
            radioBtn3.setText(currentQuestion.getOptionC());
            radioBtn4.setText(currentQuestion.getOptionD());
            questionCounter++;

            answer = false;

            btnNext.setText("Bestätigen");
            //Zeigt an wieviele Fragen es noch zu beantworten gibt
            txtViewCount.setText("Frage: " + questionCounter + "/" + (questionTotal));

        } else {
            Toast.makeText(this, "Glückwunsch Quiz beendet", Toast.LENGTH_SHORT).show();

            //Wurden alle fragen beantwortet, dann ruft er unsere Result auf
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    dataResult();
                }
            },2000);


        }
    }

    private void startQu() {
        setQuestion();

        //legen fest die verschiedenen möglichkeiten eine Frage auszuwäheln und welche Farbe der Btn dann haben soll
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) { //wenn man eine antwort auswählt

                switch (i){
                    case R.id.radioBtn1:
                        radioBtn1.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.answer_selected));
                        radioBtn2.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.default_bg_button));
                        radioBtn3.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.default_bg_button));
                        radioBtn4.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.default_bg_button));
                        break;

                    case R.id.radioBtn2:
                        radioBtn1.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.default_bg_button));
                        radioBtn2.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.answer_selected));
                        radioBtn3.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.default_bg_button));
                        radioBtn4.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.default_bg_button));
                        break;

                    case R.id.radioBtn3:
                        radioBtn1.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.default_bg_button));
                        radioBtn2.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.default_bg_button));
                        radioBtn3.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.answer_selected));
                        radioBtn4.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.default_bg_button));
                        break;

                    case R.id.radioBtn4:
                        radioBtn1.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.default_bg_button));
                        radioBtn2.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.default_bg_button));
                        radioBtn3.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.default_bg_button));
                        radioBtn4.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.answer_selected));
                        break;
                }
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                if(!answer){
                    if (radioBtn1.isChecked() || radioBtn2.isChecked() || radioBtn3.isChecked() || radioBtn4.isChecked()){
                        questionOperation();
                    }
                }
            }
        });
    }

    private void questionOperation() {
        answer = true;
        RadioButton rSelected = findViewById(radioGroup.getCheckedRadioButtonId());
        int aNumber = radioGroup.indexOfChild(rSelected) +1;
        solutionCheck(aNumber, rSelected);
    }

    /**
     * unsere solutionCheck schaut, ob die Fragen richtig sind oder falsch beantwortet wurde
     * wird eine Antwort ausgewählt und bestätigt, dann wird diese grün wenn richtig oder rot wenn falsch
     * */
    private void solutionCheck(int aNumber, RadioButton rSelected) {

        switch (currentQuestion.getAnswer()){
            case 1:
                if(currentQuestion.getAnswer() == aNumber){ //richtig
                    radioBtn1.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.answer_right));
                    answerCorrect++;

                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            setQuestion();
                        }
                    }, 600); //verzögerung

                } else { //falsch
                    radioBtn1.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.answer_wrong));
                    answerWrong++;

                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            setQuestion();
                        }
                    }, 600);
                }
                break;

            case 2:
                if(currentQuestion.getAnswer() == aNumber){ //richtig
                    radioBtn2.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.answer_right));
                    answerCorrect++;

                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            setQuestion();
                        }
                    }, 600);

                } else {  //falsch
                    radioBtn2.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.answer_wrong));
                    answerWrong++;

                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            setQuestion();
                        }
                    }, 600);
                }
                break;

            case 3:
                if(currentQuestion.getAnswer() == aNumber){ //richtig
                    radioBtn3.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.answer_right));
                    answerCorrect++;

                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            setQuestion();
                        }
                    }, 600);

                } else { //falsch
                    radioBtn3.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.answer_wrong));
                    answerWrong++;

                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            setQuestion();
                        }
                    }, 600);
                }
                break;

            case 4:
                if(currentQuestion.getAnswer() == aNumber){ //richtig
                    radioBtn4.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.answer_right));
                    answerCorrect++;

                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            setQuestion();
                        }
                    }, 600);

                } else { //falsch
                    radioBtn4.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.answer_wrong));
                    answerWrong++;

                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            setQuestion();
                        }
                    }, 600);
                }
                break;
        }
        if(questionCounter == questionTotal){
            btnNext.setText("Beenden");
        }
    }


    /**
     * die dataResult, nimmt unsere gesammt anzahl der fragen und der richtig/falsch beantworteten fragen
     * und speichert diese in einem puExtra, damit wir die Daten in der resultActivity verwenden können.
     * Danach wird diese aufgerufen*/
    private void dataResult(){
        Intent resultQuiz = new Intent(QuizActivity.this,ResultActivity.class);
        resultQuiz.putExtra("QuestionAll", (questionTotal));
        resultQuiz.putExtra("RightAnswers", answerCorrect);
        resultQuiz.putExtra("WrongAnswers", answerWrong);

        startActivity(resultQuiz);
    }

}