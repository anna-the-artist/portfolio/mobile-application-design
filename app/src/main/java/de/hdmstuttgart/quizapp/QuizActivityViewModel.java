package de.hdmstuttgart.quizapp;


import android.app.Application;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;


import java.util.List;

import de.hdmstuttgart.quizapp.database.Question;
import de.hdmstuttgart.quizapp.database.QuizRepository;

public class QuizActivityViewModel extends AndroidViewModel {

    /**
     * Unser ViweModel holt sich die Daten und lädt diese in die Datenbank, damit wenn ein Quiz gestartet wird
     * diese nicht durchgesprungen wird und irrtümlich beendet wird
     */
    private QuizRepository mRepo;

    public MutableLiveData<List<Question>> questionDataList = new MutableLiveData<>(null);

    public QuizActivityViewModel(Application application){
        super(application);
        mRepo = new QuizRepository(application);

    }

    public void loadQuestions(AppCompatActivity activity , String category){
        new Thread(new Runnable() {
            @Override
            public void run() {
                questionDataList.postValue(mRepo.getQu(category));
            }
        }).start();
    }
}
