package de.hdmstuttgart.quizapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class OverviewActivity extends AppCompatActivity implements View.OnClickListener{

    Button btnFolien;
    Button btnKonten;
    Button btnAktivPassiv;
    Button btnMWST;
    Button btnAuswikrungen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_overview);

        btnFolien = findViewById(R.id.btnFolien);
        btnKonten = findViewById(R.id.btnKonten);
        btnAktivPassiv = findViewById(R.id.btnAktivPassiv);
        btnMWST = findViewById(R.id.btnMWST);
        btnAuswikrungen = findViewById(R.id.btnAuswirkungen);

        btnFolien.setOnClickListener(this);
        btnKonten.setOnClickListener(this);
        btnAktivPassiv.setOnClickListener(this);
        btnMWST.setOnClickListener(this);
        btnAuswikrungen.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.btnFolien:
                Intent intentFolien = new Intent(OverviewActivity.this, QuizActivity.class);
                intentFolien.putExtra("Overview", "folien");
                startActivity(intentFolien);

                break;

            case R.id.btnKonten:
                Intent intentKonten = new Intent(OverviewActivity.this, QuizActivity.class);
                intentKonten.putExtra("Overview", "konten");
                startActivity(intentKonten);

                break;

            case R.id.btnAktivPassiv:
                Intent intentAkPa = new Intent(OverviewActivity.this, QuizActivity.class);
                intentAkPa.putExtra("Overview", "aktiv,passiv");
                startActivity(intentAkPa);

                break;

            case R.id.btnMWST:
                Intent intentMWST = new Intent(OverviewActivity.this, QuizActivity.class);
                intentMWST.putExtra("Overview", "mwst");
                startActivity(intentMWST);

                break;

            case R.id.btnAuswirkungen:
                Intent intentAuswirkungen = new Intent(OverviewActivity.this, QuizActivity.class);
                intentAuswirkungen.putExtra("Overview", "auswirkungen");
                startActivity(intentAuswirkungen);

                break;
        }
    }
}