package de.hdmstuttgart.quizapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class ResultActivity extends AppCompatActivity {

    TextView txtQuizQuestion;
    TextView txtAnsRight;
    TextView txtAnsWrong;

    Button btnHome;

    private int start;
    private float end;

    private float questionNumber;
    private float answPercentRight;
    private float answPercentWrong;
    private int ansNumberRight;
    private int ansNumberWrong;

    ImageView ballon;
    Animation bottom;

    ImageView graphImageRight;
    ImageView graphImageWrong;

    private Handler handler = new Handler();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        txtQuizQuestion = findViewById(R.id.question_view);
        txtAnsRight = findViewById(R.id.right_view);
        txtAnsWrong = findViewById(R.id.wrong_view);

        graphImageRight = findViewById(R.id.graph_right);
        graphImageWrong = findViewById(R.id.graph_false);

        btnHome = findViewById(R.id.btnHome);

        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ResultActivity.this, HomeActivity.class);
                startActivity(intent);
            }
        });

        Button btnGraph = findViewById(R.id.btnGraphResult);
        btnGraph.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int rightPercent = ansNumberRight;
                int wrongPercent = ansNumberWrong;

                Intent intent = new Intent(ResultActivity.this, GraphActivity.class);
                intent.putExtra("right", rightPercent);
                intent.putExtra("wrong", wrongPercent);
                startActivity(intent);
            }
        });

        Intent intent = getIntent();
        int allQue = intent.getIntExtra("QuestionAll", 0);
        int ansRight = intent.getIntExtra("RightAnswers",0);
        int ansWrong = intent.getIntExtra("WrongAnswers",0);

        txtQuizQuestion.setText("Anzahl Fragen: "+ String.valueOf(allQue));
        txtAnsRight.setText("Richtig: " + String.valueOf(ansRight));
        txtAnsWrong.setText(("Falsch: "+ String.valueOf(ansWrong)));

        //Wir rechnen die Prozent zahl für richtig aus, was auch angezeigt wird
        questionNumber = allQue;
        answPercentRight = (float) ((ansRight/ questionNumber)*100.0);
        ansNumberRight = Math.round(answPercentRight);

        //Wir rechnen hir die Prozent zahl für falsch aus, aber speichern diesen Wert nur ab
        questionNumber = allQue;
        answPercentWrong = (float) ((ansWrong/ questionNumber)*100.0);
        ansNumberWrong = Math.round(answPercentWrong);


        TextView resultAni = findViewById(R.id.result_animation);
        textViewAnimation(0, answPercentRight, resultAni);

        ballon = findViewById(R.id.ballons);
        bottom = AnimationUtils.loadAnimation(this, R.anim.bottom_anim);

        if(ansNumberRight == 100){
            Toast.makeText(this, "Glückwunsch!",Toast.LENGTH_LONG).show();
            ballon.setVisibility(View.VISIBLE);
            ballon.setAnimation(bottom);
        }
    }

    /**
     * Die textViewAnimation animiert unser Prozentales ergebnis der richtig beantworteten fragen
     */
    private void textViewAnimation(int count, float total, final TextView textView){
        start = count;
        end = total;

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (start < end){
                    try {
                        Thread.sleep(45);
                    }catch(InterruptedException e){
                        e.printStackTrace();
                    }
                    textView.post(new Runnable() {
                        @Override
                        public void run() {
                            textView.setText(""+ start);
                        }
                    });
                    start++;
                }
            }
        }).start();
    }

}