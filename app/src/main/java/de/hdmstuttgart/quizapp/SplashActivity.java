package de.hdmstuttgart.quizapp;


import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;


public class SplashActivity extends AppCompatActivity {

    /**
     * Die Splash activity zeogt beim starten der App unser Logo, damit nicht sofort das Home Interface
     * angezeigt wird
     */
    ImageView imageViewLogo;

    Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        imageViewLogo = findViewById(R.id.splash_img);

        Animation animation = AnimationUtils.loadAnimation(this, R.anim.transition_anim);
        imageViewLogo.setAnimation(animation);

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent i = new Intent(SplashActivity.this, HomeActivity.class);
                startActivity(i);
                finish();
            }
        }, 3000);

    }
}