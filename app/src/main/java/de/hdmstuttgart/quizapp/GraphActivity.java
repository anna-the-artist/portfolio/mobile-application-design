package de.hdmstuttgart.quizapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class GraphActivity extends AppCompatActivity {

    ImageView graphImageRight;
    ImageView graphImageWrong;

    /**
     * In Zeile 32 und 33 holen wir uns die Höhe der beiden ImageViews, damit wir in Zeile 35 und 36 die
     * DP von dem Handy Layout berechnen können, damit die Graphen, egal bei welchem Handy, immer sich in dem
     * Koordinaten System befinden
     *
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph);

        graphImageRight = findViewById(R.id.graph_right);
        graphImageWrong = findViewById(R.id.graph_false);

        Intent intent = getIntent();
        int rightPercentNumber = intent.getIntExtra("right", 0);
        int wrongPercentNumber = intent.getIntExtra("wrong", 0);

        int rightHeight = (int) (rightPercentNumber * graphImageRight.getContext().getResources().getDisplayMetrics().density * 3.8);
        int falseHeight = (int) (wrongPercentNumber * graphImageWrong.getContext().getResources().getDisplayMetrics().density * 3.8);


        graphImageRight.getLayoutParams().height = rightHeight;
        graphImageWrong.getLayoutParams().height = falseHeight;

        Button btnBack = findViewById(R.id.btnBack);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(GraphActivity.this, HomeActivity.class);
                startActivity(intent);
            }
        });

    }
}