package de.hdmstuttgart.quizapp.database;

import android.app.Application;
import de.hdmstuttgart.quizapp.database.QuizQuestionDao;

public class QuizApplication extends Application {

    /**
     * Damit gleich am Anfang die Datenbank gefüllt ist und
     * der onCreate handler beim ersten start die Datenbank füllt
     * */
    @Override
    public void onCreate() {
        super.onCreate();
        QuizQuestionDatabase db = QuizQuestionDatabase.getInstance(this);
        de.hdmstuttgart.quizapp.database.QuizQuestionDao mDao = db.questionDao();
        new Thread(new Runnable() {
            @Override
            public void run() {
                mDao.getAllQuestions();
            }
        }).start();


    }
}
