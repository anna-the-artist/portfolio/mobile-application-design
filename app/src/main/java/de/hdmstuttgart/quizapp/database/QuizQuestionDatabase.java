package de.hdmstuttgart.quizapp.database;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import de.hdmstuttgart.quizapp.database.QuizQuestionDao;

@Database(entities = {Question.class}, version = 1)
public abstract class QuizQuestionDatabase extends RoomDatabase {

    private static QuizQuestionDatabase INSTANCE;

    public abstract QuizQuestionDao questionDao();

    public static synchronized QuizQuestionDatabase getInstance(final Context context){
        if(INSTANCE == null){
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                        QuizQuestionDatabase.class, "questions_database")
                        .fallbackToDestructiveMigration()
                        .addCallback(RoomDataCallBack)
                        .build();

        }
        return INSTANCE;
    }

    private static RoomDatabase.Callback RoomDataCallBack = new RoomDatabase.Callback(){

        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            new PopulateDataBase(INSTANCE).execute();
        }

    };

    private static class PopulateDataBase extends AsyncTask<Void, Void, Void>{

        private QuizQuestionDao quizQuestionDao;

        private PopulateDataBase(QuizQuestionDatabase db){

            quizQuestionDao = db.questionDao();
        }

        /**
         * In doInBackground werden unsere fragen gespeichert, bzw. hier wird alles reingeschrieben
         * */
        @Override
        protected Void doInBackground(Void... voids) {

            //1. Quiz Folien 1-5
            quizQuestionDao.insert(new Question("Was ist das Rechnungswesen?", "Eine Mathematische Formel", "Eine systematische Erfassung und Darstellung aller finanz- und vermögenswirksamen Abläufe", "Einnahmen von Unternehmen","Kosten von Unternehmen",2, "folien"));
            quizQuestionDao.insert(new Question("An wen richtet sich das Rechnungswesen?", "Gemeinde", "Staat und Regierung", "Steuerverwaltung, Managment und Auftraggeber","Unternehmensleitung",3, "folien"));
            quizQuestionDao.insert(new Question("Aufgaben des Rechnungswesens:", "Planung und Vorbereitung von unternehmerischen Entscheidungen", "Dokumentation der betrieblichen Aufträgen", "Ermittlung der aktuellen Angestellten","Berechnung der möglichen Kosten für ein Projekt",1, "folien"));
            quizQuestionDao.insert(new Question("Die Buchführung erfasst aufgrund von Belegen alle:", "aktuellen Aufträge", "Aufwendungen und Erträge des Unternehmens", "Mitarbeiter und Lieferanten","Kunden",2, "folien"));
            quizQuestionDao.insert(new Question("Was für arten der Buchführung gibt es?", "Die dreifache Buchführung – Depik", "Die einfache Buchführung – Tepik", "Die vierfache Buchführung – Topik","Die doppelte Buchführung – Dopik",4, "folien"));
            quizQuestionDao.insert(new Question("Die Buchführung muss ordnungsgemäß…", "Unterlagen nach 6 Monaten vernichten", "Lieferanten angeben", "Klar und übersichtlich sein","nur das Unternehmen verstehen können",3, "folien"));
            quizQuestionDao.insert(new Question("Was ist eine Inventur?", "Mengen- und Bestandsaufnahme aller Vermögensteile und Schulden in einem Unternehmen", "Mengen- und Bestandsaufnahme aller Produkte des Unternehmens", "Auflistung mit allen Gegenständen des Betriebes","Ein anderes Wort für Betriebsaufnahme",1, "folien"));
            quizQuestionDao.insert(new Question("Was ist eine Bilanz?", "Dient dazu das Eigenkapital zu ermitteln", "Dient dazu sämtliche Schulden des Jahres aufzulisten", "Zusammengefasste strukturierte Gegenüberstellung des Vermögens und seiner Finanzierung durch Eigenkapital und Schulden","Das Erstellen einer Übersicht aller eingenommener Gewinne",3, "folien"));
            quizQuestionDao.insert(new Question("Was bedeutet AfA?", "Abschreibung für Anschaffungen", "Anschreibung für Anschaffungen", "Abschreibung für Abnutzung","Anschreibung für Abnutzung",3, "folien"));
            quizQuestionDao.insert(new Question("Im Einkaufspreis Waren und Dienstleistungen ist MWST erhalten", "Vorsteuer wird ausgegeben", "Vorsteuer wird gewonnen", "Vorsteuer wird eingenommen","Vorsteuer wird bezahlt",4, "folien"));
            quizQuestionDao.insert(new Question("Im Verkaufspreis von Waren und Dienstleistungen ist MWST enthalten", "Umsatzsteuer wird gewonnen", "Umsatzsteuer wird eingenommen", "Umsatzsteuer wird ausgegeben","Umsatzsteuer wird bezahlt",2, "folien"));
            quizQuestionDao.insert(new Question("Was ist ein Grundsatz der Kostenstellenbildung?", "Keine Wirtschaftlichkeit", "Schaffung selbständiger Verantwortungsbereiche", "Vorgegebene Bezugsgrößen","Eindeutigkeit und Klarheit nur für das Unternhemen",2, "folien"));

            //2. Quiz Konten fragen
            quizQuestionDao.insert(new Question("Zu welchem Konto gehört Kasse?", "Aufwandskonto", "Ertragskonto", "Fremdkapitalkonto","Vermögenskonto",4, "konten"));
            quizQuestionDao.insert(new Question("Zu welchem Konto gehört Bank?", "Aufwandskonto", "Ertragskonto", "Fremdkapitalkonto","Vermögenskonto",4, "konten"));
            quizQuestionDao.insert(new Question("Zu welchem Konto gehört Verbindlichkeiten?", "Aufwandskonto", "Ertragskonto", "Fremdkapitalkonto","Vermögenskonto",3, "konten"));
            quizQuestionDao.insert(new Question("Zu welchem Konto gehört Forderungen?", "Aufwandskonto", "Ertragskonto", "Fremdkapitalkonto","Vermögenskonto",4, "konten"));
            quizQuestionDao.insert(new Question("Zu welchem Konto gehört Gehälter?", "Aufwandskonto", "Ertragskonto", "Fremdkapitalkonto","Vermögenskonto",1, "konten"));

            //3. Quiz Aktiva, Passiva
            quizQuestionDao.insert(new Question("Was wird bei einem Aktivkonto auf der Soll seite gemacht?", "Hier steht der Anfangsbestand", "Hier steht der Schlussbestand", "Hier findet die Abnahme statt","Hier ist der Gewinn und Verlust",1, "aktiv,passiv"));
            quizQuestionDao.insert(new Question("Was wird bei einem Aktivkonto auf der Haben seite gemacht?", "Hier steht der Anfangsbestand", "Hier steht der Schlussbestand", "Hier findet die Zunahme statt","Hier ist der Gewinn und Verlust",2, "aktiv,passiv"));
            quizQuestionDao.insert(new Question("Was wird bei einem Passivkonto auf der Soll seite gemacht?", "Hier steht der Anfangsbestand", "Hier steht der Schlussbestand", "Hier findet die Zunahame statt","Hier ist der Gewinn und Verlust",2, "aktiv,passiv"));
            quizQuestionDao.insert(new Question("Was wird bei einem Passivkonto auf der Haben seite gemacht?", "Hier steht der Anfangsbestand", "Hier steht der Schlussbestand", "Hier findet die Abnahme statt","Hier ist der Gewinn und Verlust",1, "aktiv,passiv"));

            //4. Quiz Rechnungen MWST
            quizQuestionDao.insert(new Question("Mit welcher MWSTrechnen wir?", "10,7 %", "20 %", "7 %","19 %",4, "mwst"));
            quizQuestionDao.insert(new Question("Was ist die Formel für die MWST?", "Endpreis * 0,19", "Endpreis : 1,19 : 100 * 19", "Endpreis * 1,19","Endpreis : 0,19 * 100",2, "mwst"));
            quizQuestionDao.insert(new Question("Wie viel MWST haben sie bei 998€ bezahlt?", "189,62 €", "122,83 €", "159,34 €","168,37 €",3, "mwst"));

            //5. Quiz Auswirkungen auf Geschäftsvorfälle auf das Betriebsergebnis
            quizQuestionDao.insert(new Question("Gehaltszahlung bar", "Steigung", "Neutral", "Reduktion","Insolvenz",3, "auswirkungen"));
            quizQuestionDao.insert(new Question("Bazahlung der Büromiete durch Banküberweisung", "Steigung", "Neutral", "Reduktion","Insolvenz",3, "auswirkungen"));
            quizQuestionDao.insert(new Question("Kunde begleicht Rechnung durch Banküberweisung", "Steigung", "Neutral", "Reduktion","Insolvenz",2, "auswirkungen"));
            quizQuestionDao.insert(new Question("Verkauf einer Lizenz unseres Produkts", "Steigung", "Neutral", "Reduktion","Insolvenz",1, "auswirkungen"));
            quizQuestionDao.insert(new Question("Bezhalung einer Lieferantenrechnung 60 Tage nach Rechnungseingang", "Steigung", "Neutral", "Reduktion","Insolvenz",2, "auswirkungen"));

            return null;
        }
    }
}
