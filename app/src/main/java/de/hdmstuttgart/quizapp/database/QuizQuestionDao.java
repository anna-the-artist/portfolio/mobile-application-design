package de.hdmstuttgart.quizapp.database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import de.hdmstuttgart.quizapp.database.Question;

@Dao
public interface QuizQuestionDao {

    //Alle Daten holen
    @Query("SELECT * from questionTable")
    List<Question> getAllQuestions();

    //Alle daten holen die mit dem String übereinstimmen
    @Query("SELECT * from questionTable WHERE questionTable.overview = :overview")
    List<Question> getOverviewQuestions(String overview);

    @Insert
    void insert(Question question);
}
