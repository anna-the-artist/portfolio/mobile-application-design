package de.hdmstuttgart.quizapp.database;

import android.app.Application;

import java.util.List;

import de.hdmstuttgart.quizapp.database.Question;
import de.hdmstuttgart.quizapp.database.QuizQuestionDatabase;

public class QuizRepository {

    private QuizQuestionDao mDao;
    private List<Question> mAll;

    public QuizRepository(Application application){
        QuizQuestionDatabase db = QuizQuestionDatabase.getInstance(application);
        mDao = db.questionDao();
    }

    public  List<Question> getQu(String overview){
        mAll = mDao.getOverviewQuestions(overview);
        return mAll;
    }

}
