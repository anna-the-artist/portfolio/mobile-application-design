# Mobile Application Development1 project BWL Quiz App

## Project abstract

Choosed topics: Animation and Graphics, Date Storage

Our project is about a learning app with which you can learn business administration.
The knowledge is tested through various quizzes and the progress of the topics is displayed in a graphic.
If a topic is answered 100% correctly, the user is rewarded with an animation.


## How the app works:

When the app is started, there is a Quiz info button. Here it is explained that there is always only one possible answer. Below that is the button to select a quiz from five categories. 
Once you have chosen a category, there are different questions with four possible answers. Once you have answered all the questions, the result of the quiz is displayed. From there, you can return to the Home Screen or view the result again visually in a graph.

## Problems encountered:

As soon as you start the app and select a quiz, it is immediately scrolled through and closed. Our problem was that the database was not yet filled. To solve the problem, we filled the database while the little intro at the beginning was playing.

Another problem was our graph. Since we inserted it as an image, our bars no longer corresponded to the image when you have different screen sizes. To prevent this from happening again, we have the screen size calculated each time and the result is then used for the image size.

Our last problem that caused us difficulties was our UI tests. Every time a test is supposed to run, an error message is displayed indicating that it cannot start the test. With the help of the tutor, we were able to fix this problem. But by fixing this error, a new one has appeared. This time all tests failed because the programme was too fast for the tests. With the help of sleep functions, we were able to fix this error.


## What can be extended:

You could add more categories and more types of questions and answers, e.g. that you can answer with text.


#### Team members:  

Katharina S.  
Annalena H.
